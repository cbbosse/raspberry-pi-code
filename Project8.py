# program to use a button to take a picture

# import control, camera, date, and timing libraries
from gpiozero import Button
import picamera
import datetime
from signal import pause

# set up the button to take a picture
button1 = Button(17)
button2 = Button(6)
camera = picamera.PiCamera()

# define a function to open the preview window and take a picture
def capture():
    stamp = datetime.datetime.now().isoformat()
    camera.capture('/home/pi/%s.jpg' % stamp)
    
# start the preview window
button2.when_pressed = camera.start_preview
button2.when_released = camera.stop_preview
    
# take the picture
button1.when_pressed = capture

pause()
