# project to take a picture and send it in an email

# warning about the need for a gmail account
print "This only works with gmail accounts as the sender address!"  

# import GPIO pin control, time commands, special Linux commands, camera commands, and simple message commands  
import RPi.GPIO as GPIO  
import time  
import os
import picamera
import smtplib

# import sub-modules related to email, MIME allows for attachments 
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders


 
# determines which naming set to use, set up pin 18 to pull down (go low)  
GPIO.setmode(GPIO.BCM)  
GPIO.setup(18, GPIO.IN, pull_up_down = GPIO.PUD_UP)  
 
# function on what to do when the button is pressed  
def takephoto(channel):
        # setup a naming convention for the photos, and the camera setup
        photo_time = str(time.time())
        name = photo_time + ".jpg"
	camera = picamera.PiCamera()
	camera.capture(name)
	camera.close()

        # request the to and from addresses
        fromaddr = raw_input("Please enter your gmail address: ")
        toaddr = raw_input("Please enter the address you are sending to: ")
    
        msg = MIMEMultipart()
 
        # setup the email header information
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = raw_input("Please enter the subject: ")

        # create and attach the body of the email    
        body = raw_input("Please enter a brief message: ")
        msg.attach(MIMEText(body, 'plain'))

        # establish the photo attachment, and the file specifications
        filename = name
        directory = "/home/pi/Projects/" + filename
        attachment = open(directory, "rb")
 
        part = MIMEBase('application', 'octet-stream')
        part.set_payload((attachment).read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "attachment; filename = %s" % filename)
 
        msg.attach(part)
 
        # open the connection with the email server 
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(fromaddr, raw_input("Please enter your password: "))
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
 
# when the button is pressed, start the function to take a picture and send the email
print "Press the button to take a picture"
GPIO.add_event_detect(18, GPIO.FALLING, callback = takephoto, bouncetime = 2000)  
 
# wait until the button is pressed  
while 1:  
    time.sleep(1)  
