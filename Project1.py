﻿# program to turn on an LED on a breadboard

# import library code specific to the Raspberry Pi's Input/Output pins, refer to it by a local name, GPIO
import RPi.GPIO as GPIO

# import commands related to time
import time

# turns off warning messages, determines which naming set to use, and directs the Pi to use pin 18
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)

# displays "LED On", turns power on to pin 18, pauses briefly, reverses the process
print "LED On"
GPIO.output(18,GPIO.HIGH)
time.sleep(1)
print "LED Off"
GPIO.output(18,GPIO.LOW)

# sets all pins back to the default
GPIO.cleanup()