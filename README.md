# Welcome! #

This is a site for my Raspberry Pi project. The codes are the ones used to introduce the Raspberry Pi to Computer Information Systems, Computer Science, Cyber Security, Forensics, and Networking students. It can be used to show students the operating system, programming, and physical computing.

https://bitbucket.org/cbbosse/raspberry-pi-code/wiki/Home