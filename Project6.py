# project to turn a motor on/off, and faster/slower

# import Raspberry Pi GPIO pin commands, and commands for time
import RPi.GPIO as GPIO   
import time       

# do not show any warnings, determine the naming set to use for the pins, set up pins as input or output
# sets pin 13 as an output pin, reads from pin 19 from button 1 and pin 26 from button 2
GPIO.setwarnings(False)         	
GPIO.setmode(GPIO.BCM)           
GPIO.setup(13,GPIO.OUT)          
GPIO.setup(19,GPIO.IN)           
GPIO.setup(26,GPIO.IN)           

# initial power to the motor to start it, pin 13 is set to 100% of duty cycle
# the program is paused, and then stepped down to 75%
x=0                              
p = GPIO.PWM(13,100)        	 
p.start(100)                     
time.sleep(2)                    
p.start(75)                      

# loop that increases or decreases the duty cycles by 10 depending on whether button 1 or 2 is pushed
# prints the value of the duty cycle on the screen
while 1:                         
   p.ChangeDutyCycle(x)          
   if(x<50):
      x=x+10                  
      time.sleep(0.2)         
      print "pwm %d" % (x)                 
   if(GPIO.input(19) == False):  
      if(x>0):
         x=x-10                  
         time.sleep(0.2)         
         print "pwm %d" % (x)    

GPIO.cleanup()
