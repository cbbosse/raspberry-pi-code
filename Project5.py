# program to read the amount of light in the room

# import Raspberry Pi GPIO pin commands, Linux OS specific commands, and commands for time
import RPi.GPIO as GPIO
import os      
import time

# determine which pin naming system to use 
# DEBUG = 1
GPIO.setmode(GPIO.BCM)
 
# define a function to read the level of light in the room
def RCtime (RCpin):
        reading = 0
        GPIO.setup(RCpin, GPIO.OUT)
        GPIO.output(RCpin, GPIO.LOW)
        time.sleep(0.1)
 
        GPIO.setup(RCpin, GPIO.IN)
        
        while (GPIO.input(RCpin) == GPIO.LOW):
                reading += 1
        return reading

# loop to read the light until the program is closed
# test the level of light, and if low enough turn on the light
# pin 4 reads the light, pin 18 turns on the LED
# accepts ctrl-c to stop the program execution and restore pins to default values
try: 
	while True:
        	on = RCtime(4)
        	print on  
        	if on > 1000:
                	GPIO.setup(18, GPIO.OUT)
                	print "LED On"
                	GPIO.output(18,GPIO.HIGH)
        	else:
                	GPIO.output(18, GPIO.LOW)
                	print "LED Off"

except  KeyboardInterrupt:
	pass

finally:
        GPIO.cleanup()
