# program to monitor the temperature in the room

# import Linux OS specific commands, file path names,  advanced Python commands, and commands for time
import os 
import glob
import sys
import time

# Load the 1-wire modules as part of the Linux kernel to probe temperatures
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

# define variables, these are files in the OS folders related to devices
base_dir = '/sys/bus/w1/devices/'
devices = glob.glob(base_dir + '28*')
device_file = '/w1_slave'

# defines a function to read the file where the temperature readings are stored
def read_temp_file(device_file):
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

# defines a function to read the device and store the data in a file
# prints the reading in Celsius on the screen 
def read_all():
    for  device in devices:
        device_dir = device + device_file
        raw_data = read_temp_file(device_dir)

        while raw_data[0].find('YES') == -1:
            sleep(0.1)
            raw_data = read_temp_file(device_dir)

        t_pos = raw_data[1].find('t=')

        if t_pos != -1:
            temp = float(raw_data[1][t_pos+2:]) / 1000
            print('%s - %.1f%s' % (device, temp, u"\u00b0"+'C'))

# loop to read the temperature from the sensor as long as it can
# stops the program when ctrl-c is pressed
try:
    while True:
        read_all()
        time.sleep(1)
except KeyboardInterrupt:
    # quit
    print('Exiting application')

