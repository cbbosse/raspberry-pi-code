# program to use a button to take a picture

# import control, camera, date, and timing libraries
from gpiozero import Button
import picamera
import datetime
from signal import pause

# set up the button to take a picture
button = Button(17)
camera = picamera.PiCamera()

# define a function to take a picture
def capture():
    stamp = datetime.datetime.now().isoformat()
    camera.capture('/home/pi/%s.jpg' % stamp)
    
# take the picture
button.when_pressed = capture

pause()
