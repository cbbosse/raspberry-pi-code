# project to turn on/off an LED using a button

# import library code specific to the Raspberry Pis Input/Output pins, 
# refer to it by a local name, GPIO
import RPi.GPIO as GPIO

# import commands related to time
import time

# turns off warning messages, determines which naming set to use
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

#define pin names
switch_pin = 18
led_pin = 23

#detects whether the LED is on or off, and if on, turns it off, or if off turns it on
GPIO.setup(switch_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(led_pin, GPIO.OUT)
led_state = False
old_input_state = True # pulled-up

# loop that monitors the pushing of the button to turn on/off the LED
while True:
	new_input_state = GPIO.input(switch_pin)
	if new_input_state == False and old_input_state == True:
		led_state = not led_state
	old_input_state = new_input_state
	GPIO.output(led_pin, led_state)

# sets all pins back to the default
GPIO.cleanup()
