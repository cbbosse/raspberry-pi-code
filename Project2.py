# program to make an LED on the breadboard blink

# import library code specific to the Raspberry Pis Input/Output pins, refer to it by a local name, GPIO
import RPi.GPIO as GPIO

# import commands related to time
import time

# turns off warning messages, determines which naming set to use
# and directs the Pi to use pin 18 an output pin
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)

# defines a function to turn on/off an LED
def blink(pin):
        GPIO.output(pin,GPIO.HIGH)
        time.sleep(1)
	print "Lights On"
        GPIO.output(pin,GPIO.LOW)
        time.sleep(1)
	print "Lights Off"
        return


# code that starts the actual blinking, range specifies how many times the led blinks
for i in range(0,5):
        blink(18)

# sets all pins back to the default
GPIO.cleanup()


